<?php
namespace Bairwell\Autoloader\Tests;

class AutoloaderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \Bairwell\Autoloader
     */
    private $autoloader;

    public function setUp()
    {
        $this->autoloader = new \Bairwell\Autoloader();
        if (($this->autoloader instanceof \Bairwell\Autoloader) === FALSE) {
            $this->fail('Could not load autoloader');
        }
        spl_autoload_register(array($this->autoloader, 'load'));
    }

    public function tearDown()
    {
        spl_autoload_unregister(array($this->autoloader, 'load'));
    }

    public function testPrefixesEmpty()
    {
        $this->assertTrue(is_array($this->autoloader->getPrefixes()));
        $this->assertEmpty($this->autoloader->getPrefixes());
    }

    public function testAddPrefixFail()
    {
        $thisDir = 'FRED';
        $emess = NULL;
        try {
            $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('FRED does not appear to be a directory', $emess);
    }

    public function testAddPrefixWorks()
    {
        $this->assertTrue(is_array($this->autoloader->getPrefixes()));
        $this->assertEmpty($this->autoloader->getPrefixes());
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $prefixes = $this->autoloader->getPrefixes();
        $this->assertTrue(is_array($prefixes));
        $this->assertEquals(1, count($prefixes));
        $this->assertTrue(isset($prefixes['Bairwell\Autoloader\Tests\Fixtures']));
        $this->assertTrue(is_array($prefixes['Bairwell\Autoloader\Tests\Fixtures']));
        $this->assertEquals(1, count($prefixes['Bairwell\Autoloader\Tests\Fixtures']));
        $this->assertEquals($thisDir, $prefixes['Bairwell\Autoloader\Tests\Fixtures'][0]);
    }

    public function testAddMultiplePrefixWorks()
    {
        $this->assertTrue(is_array($this->autoloader->getPrefixes()));
        $this->assertEmpty($this->autoloader->getPrefixes());
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures\Depth\Here', $thisDir);
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures\More', $thisDir);
        $this->autoloader->addPrefix('Awagoo\Awagoo\Fixtures\More', $thisDir);
        $this->autoloader->addPrefix('Awagoo\Awagoo\Fixtures\More', __DIR__);
        $prefixes = $this->autoloader->getPrefixes();
        $this->assertTrue(is_array($prefixes));
        $this->assertEquals(4, count($prefixes));
        $expected = Array(
            'Awagoo\Awagoo\Fixtures\More' => Array($thisDir, __DIR__),
            'Bairwell\Autoloader\Tests\Fixtures' => Array($thisDir),
            'Bairwell\Autoloader\Tests\Fixtures\More' => Array($thisDir),
            'Bairwell\Autoloader\Tests\Fixtures\Depth\Here' => Array($thisDir)

        );
        $diff = array_diff($expected, $prefixes);
        $this->assertEquals(0, count($diff));
    }

    public function testLoadAlreadyExists()
    {
        $emess = NULL;
        try {
            $this->autoloader->load('Exception');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Class already loaded: Exception', $emess);
    }

    public function testLoadFileNotFound()
    {
        $result=$this->autoloader->load('This\Is\A\Madeup\Class');
        $this->assertFalse($result);
    }

    public function testLoadFileKnownNamespace()
    {
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $loaded = $this->autoloader->getLoaded();
        $this->assertTrue(is_array($loaded));
        $this->assertEquals(0, count($loaded));
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->assertFalse(class_exists('Bairwell\Autoloader\Tests\Fixtures\Tester', FALSE),
            'Should not already be loaded');
        $result = $this->autoloader->load('Bairwell\Autoloader\Tests\Fixtures\Tester');
        $this->assertTrue($result);
        $this->assertTrue(class_exists('\Bairwell\Autoloader\Tests\Fixtures\Tester', FALSE), 'Should now be loaded');
        $loaded = $this->autoloader->getLoaded();
        $this->assertTrue(is_array($loaded));
        $this->assertEquals(1, count($loaded));
    }

    public function testLoadFileKnownNamespaceUsingSPL()
    {
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->assertFalse(class_exists('Bairwell\Autoloader\Tests\Fixtures\TestSPL', FALSE));
        \Bairwell\Autoloader\Tests\Fixtures\TestSPL::demo(123);
        $this->assertTrue(class_exists('Bairwell\Autoloader\Tests\Fixtures\TestSPL', FALSE));
    }

    public function testLoadFileDeepKnownNamespace()
    {
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->autoloader->addPrefix('Random\Tests\Fixtures', $thisDir);
        $this->assertFalse(class_exists('Bairwell\Autoloader\Tests\Fixtures\Tester2', FALSE));
        $this->assertFalse(class_exists('Random\Tests\Fixtures\Deeper\Deep', FALSE));
        $result = $this->autoloader->load('Random\Tests\Fixtures\Deeper\Deep');
        $this->assertTrue($result);
        $this->autoloader->load('Bairwell\Autoloader\Tests\Fixtures\Tester2');
        $this->assertTrue($result);
        $this->assertTrue(class_exists('Random\Tests\Fixtures\Deeper\Deep', FALSE));
        $this->assertTrue(class_exists('Bairwell\Autoloader\Tests\Fixtures\Tester2', FALSE));
    }

    public function testLoadFileBadClass()
    {
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $emess = NULL;
        try {
            $this->autoloader->load('Bairwell\Autoloader\Tests\Fixtures\NotCorrect');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Unable to load class Bairwell\Autoloader\Tests\Fixtures\NotCorrect', $emess);
    }

    public function testLoadNonNamespaced()
    {
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';
        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->assertFalse(class_exists('PHPUnit_Framework_TestSuite_DataProvider', FALSE));
        /**
         * Try and load a PHP unit class we should not be using
         */
        $this->autoloader->load('PHPUnit_Framework_TestSuite_DataProvider');
        $this->assertTrue(class_exists('PHPUnit_Framework_TestSuite_DataProvider', FALSE));

    }

    /**
     * We should not raise exceptions for file not found classes starting PHPUnit_Extensions
     */
    public function testLoadFileNotFoundWorkaroundForPHPUnitBug()
    {
        $result = $this->autoloader->load('PHPUnit_Extensions_Dummy_Dummy_NotReal');

        $this->assertFalse($result);
    }


    public function testDebugModeEnabled()
    {
        $this->assertNull($this->autoloader->getDebugStack(), 'Debug stack should be empty');
        $result = $this->autoloader->getDebugState();
        $this->assertFalse($result, 'Debug mode should be false by default');
        $this->autoloader->setDebugState(TRUE);
        $result = $this->autoloader->getDebugState();
        $this->assertTrue($result, 'Debug mode should be set to true by now');
        /**
         * Now do the loading
         */
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';

        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $this->assertFalse(class_exists('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug', FALSE));

        $result = $this->autoloader->load('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug');
        $this->assertTrue($result);
        $this->assertTrue(class_exists('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug', TRUE));
        /**
         * And make sure everything is back to normal
         */
        $this->autoloader->setDebugState(FALSE);
        $result = $this->autoloader->getDebugState();
        $this->assertFalse($result, 'Debug mode should have been reset to false');
        /**
         * No further output should be produced
         */
        $this->assertFalse(class_exists('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug2', FALSE));

        $result = $this->autoloader->load('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug2');
        $this->assertTrue($result);
        $this->assertTrue(class_exists('Bairwell\Autoloader\Tests\Fixtures\TesterForDebug2', TRUE));
        $expectedOutput =
            'Added ' . $thisDir . ' to Bairwell\Autoloader\Tests\Fixtures' . "\n" .
                'Attempting to find Bairwell\Autoloader\Tests\Fixtures\TesterForDebug' . "\n" .
                '  - Looking for old style classname TesterForDebug' . "\n" .
                '  - Replaced classname : TesterForDebug' . "\n" .
                ' - Trying to match ' . $thisDir . '/TesterForDebug.php' . "\n" .
                ' - Found as ' . $thisDir . '/TesterForDebug.php' . "\n" .
                'Returning ' . $thisDir . '/TesterForDebug.php' . "\n";
        $this->assertEquals($expectedOutput, $this->autoloader->getDebugStack());
    }

    /**
     * @outputBuffering enabled
     */
    public function testDebugModeEnabledUnableToLocate()
    {
        $this->assertNull($this->autoloader->getDebugStack(), 'Debug stack should be empty');
        $result = $this->autoloader->getDebugState();
        $this->assertFalse($result, 'Debug mode should be false by default');
        $this->autoloader->setDebugState(TRUE);
        $result = $this->autoloader->getDebugState();
        $this->assertTrue($result, 'Debug mode should be set to true by now');
        /**
         * Now do the loading
         */
        $thisDir = __DIR__ . DIRECTORY_SEPARATOR . 'Fixtures';

        $this->autoloader->addPrefix('Bairwell\Autoloader\Tests\Fixtures', $thisDir);
        $emess = NULL;
        try {
            $result = $this->autoloader->load('Made_Up_Class');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        /**
         * And make sure everything is back to normal
         */
        $this->autoloader->setDebugState(FALSE);
        $result = $this->autoloader->getDebugState();
        $this->assertFalse($result, 'Debug mode should have been reset to false');
        /**
         * Now found out what we expected to receive
         */
        $expectedOutput =
            'Added ' . $thisDir . ' to Bairwell\Autoloader\Tests\Fixtures' . "\n" .
                'Attempting to find Made_Up_Class' . "\n" .
                ' - Looking in include path' . "\n" .
                '  - Looking for old style classname Made_Up_Class' . "\n" .
                '  - Replaced classname : Made/Up/Class' . "\n";
        $exploded = explode(PATH_SEPARATOR, get_include_path());
        foreach ($exploded as $path) {
            $expectedOutput .= ' - Checking ' . $path . '/Made/Up/Class.php' . "\n";
        }
        $expectedOutput .= 'Unable to locate class Made_Up_Class' . "\n";
        $this->assertEquals($expectedOutput, $this->autoloader->getDebugStack());
    }

    /**
     * Fix to bug tracker issue #1
     *
     * Code was finding the first namespace separator and not the last
     *
     */
    public function testIssue1_WrongSeparator()
    {
        $output = $this->autoloader->classToFile('Bairwell\NameAlternatives\GB\Forenames');
        $this->assertEquals('Bairwell' . DIRECTORY_SEPARATOR . 'NameAlternatives' .
            DIRECTORY_SEPARATOR . 'GB' . DIRECTORY_SEPARATOR . 'Forenames.php', $output);
    }
}