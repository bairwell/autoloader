<?php
/**
 * This is the main bootstrap used by Bairwell test items
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Autoloader
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */

/**
 * PHP_MAJOR_VERSION isn't defined until 5.2.7, so if it isn't defined, we know we're running on an old version
 */
if (defined('PHP_MAJOR_VERSION') === FALSE) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion(
    ));
}
if (PHP_MAJOR_VERSION === 5 && PHP_MINOR_VERSION < 3) {
    die('We require PHP 5.3.0 at a minimum as we use namespaces and late static bindings. You have PHP ' . phpversion(
    ));
}
if (defined('MB_OVERLOAD_STRING') === FALSE) {
    die('We require that PHP has mbstring installed: http://php.net/mbstring.installation.php');
}
/**
 * We use UTF-8 encoding
 */
mb_internal_encoding('UTF-8');

/**
 * Load the autoloader
 */
#set_include_path('.' . PATH_SEPARATOR . get_include_path());
$dirname = dirname(__FILE__);
include_once($dirname . '/../../php/Bairwell/Autoloader.php');
/**
 * As we are testing the autoloader, dont' configure it
 *
 * \Bairwell\Autoloader::setPrefix('Bairwell\\Tests\\',$dirname. '/php');
 * \Bairwell\Autoloader::setPrefix('Bairwell\\',$dirname. '/../../php');
 * spl_autoload_register('\Bairwell\Autoloader::load');
 **/