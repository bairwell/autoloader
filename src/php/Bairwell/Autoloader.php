<?php
/**
 * This is the main Autoloader class used by Bairwell items.
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage Autoloader
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell;

/**
 * A PSR-0 autoloader based on https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md .
 */
class Autoloader
{

    /**
     * Our list of loading location prefixes.
     *
     * @var array
     */
    private $_prefixes = array();

    /**
     * What classes have we ourselves loaded?
     *
     * @var array
     */
    private $_loaded = array();

    /**
     * Are we operating in debug mode? (i.e. should we show attempting loading paths?)
     *
     * @var bool
     */
    private $_debugState = FALSE;

    /**
     * The list of debug messages in order
     *
     * @var string
     */
    private $_debugStack;

    /**
     * Gets the list of prefixes
     * @return array
     */
    public function getPrefixes()
    {
        return $this->_prefixes;
    }

    /**
     * Gets the list of loaded classes
     * @return array
     */
    public function getLoaded()
    {
        return $this->_loaded;
    }

    /**
     * Adds a prefix for loading.
     *
     * @throws \Exception If the path provided is not a valid directory
     * @param string $name The prefix/namespace we are setting a path for
     * @param string $path The filesystem path we are setting
     * @return void
     */
    public function addPrefix($name, $path)
    {
        $directory = realpath(rtrim($path, DIRECTORY_SEPARATOR));
        if (is_dir($directory) === FALSE) {
            throw new \Exception($path . ' does not appear to be a directory');
        }
        $this->_prefixes[$name][] = $directory;
        $this->_debug('Added ' . $directory . ' to ' . $name);
        ksort($this->_prefixes);
    }

    /**
     * Actually loads in a class.
     *
     * @throws \Exception
     * @param string $classname The class we are loading
     * @return boolean True if loaded, False if not loaded (an Exception will have been raised in majority of cases
     */
    public function load($classname)
    {
        if (class_exists($classname, FALSE) === TRUE) {
            throw new \Exception('Class already loaded: ' . $classname);
        }
        $file = $this->find($classname);
        if ($file === FALSE) {
            return FALSE;
        }
        include $file;
        if (class_exists($classname, FALSE) === FALSE && interface_exists($classname, FALSE) === FALSE) {
            throw new \Exception('Unable to load class ' . $classname);
        }
        $this->_loaded[$classname] = $file;
        return TRUE;
    }

    /**
     * Find the file.
     *
     * @param string $className The class name we are looking for
     * @return bool|string The file location path
     */
    public function find($className)
    {
        $matched = FALSE;
        $this->_debug('Attempting to find ' . $className);
        $matched = $this->_findInPrefixes($className);
        if ($matched === FALSE) {
            $matched = $this->_findInInclude($className);
        }
        if ($matched === FALSE) {
            $this->_debug('Unable to locate class ' . $className);
        } else {
            $this->_debug('Returning ' . $matched);
        }
        return $matched;
    }


    /**
     * Look in the prefixes for a class
     * @param string $className The class name
     * @return bool|string The file location path. FALSE if not found
     */
    private function _findInPrefixes($className)
    {
        $matched = FALSE;
        foreach ($this->_prefixes as $prefix => $paths) {
            $prefixLength = mb_strlen($prefix);
            $match = mb_substr($className, 0, $prefixLength);
            if ($match === $prefix) {
                $justThePrefix = mb_substr($className, ($prefixLength + 1));
                $file = $this->classToFile($justThePrefix);
                foreach ($paths as $path) {
                    $fullFile = $path . DIRECTORY_SEPARATOR . $file;
                    $this->_debug(' - Trying to match ' . $fullFile);
                    if (file_exists($fullFile) === TRUE) {
                        $this->_debug(' - Found as ' . $fullFile);
                        $matched = $fullFile;
                        break;
                    }
                }
            }
        }
        return $matched;
    }

    /**
     * Look in the include path for a class
     * @param string $className The class name
     * @return bool|string The file location path. FALSE if not found
     */
    private function _findInInclude($className)
    {
        $matched = FALSE;
        $this->_debug(' - Looking in include path');
        /**
         * If we've got here, we haven't been able to find the file.
         *
         * Try and load it from the include path
         */
        $filename = $this->classToFile($className);
        $exploded = explode(PATH_SEPARATOR, get_include_path());
        foreach ($exploded as $path) {
            $fullFile = $path . DIRECTORY_SEPARATOR . $filename;
            $this->_debug(' - Checking ' . $fullFile);
            if ((file_exists($fullFile) === TRUE) && is_readable($fullFile) === TRUE) {
                $this->_debug(' - Found as ' . $fullFile);
                $matched = $fullFile;
                break;
            }
        }
        return $matched;
    }

    /**
     * PSR-0 class to file converter.
     *
     * https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
     *
     * @param string $classname Classname
     * @return string The file path we should try
     */
    public function classToFile($classname)
    {
        $namespaceSeparator = '\\';
        $namespace = '';
        $class = '';
        /**
         * Find the last namespace separator.
         */
        $pos = mb_strrpos($classname, $namespaceSeparator);
        if ($pos === FALSE) {
            /**
             * There is no namespace, we are looking for an old style class.
             */
            $class = $classname;
            $this->_debug('  - Looking for old style classname ' . $class);
        } else {
            $namespace = mb_substr($classname, 0, $pos);
            $this->_debug('  - Converting ' . $classname . ' to namespace ' . $namespace);
            $namespace = str_replace($namespaceSeparator, DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
            $this->_debug('  - After replacing ' . $namespaceSeparator . ' with ' . DIRECTORY_SEPARATOR . ': ' . $namespace);
            $class = mb_substr($classname, ($pos + 1));
            $this->_debug('  - Classname : ' . $classname);
        }
        $replacedClass = str_replace('_', DIRECTORY_SEPARATOR, $class);
        $this->_debug('  - Replaced classname : ' . $replacedClass);
        $file = $namespace . $replacedClass . '.php';
        return $file;
    }

    /**
     * Sets the debug state
     *
     * @param boolean $debugState
     */
    public function setDebugState($debugState)
    {
        $this->_debugState = $debugState;
    }

    /**
     * Gets the debug state
     *
     * @return boolean
     */
    public function getDebugState()
    {
        return $this->_debugState;
    }

    /**
     * Adds a debug message if debugging is enabled
     * @param string $message The message to display
     */
    private function _debug($message)
    {
        if ($this->_debugState === TRUE) {
            $this->_debugStack .= $message . "\n";
        }
    }

    /**
     * Gets the debug stack
     * @return string
     */
    public function getDebugStack()
    {
        return $this->_debugStack;
    }

}