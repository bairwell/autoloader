Autoloader
================
This a module written in PHP 5.3.1+ (and, therefore, requires Namespaces support) which autoloads
classes.

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd. Whilst Bairwell Ltd
holds the copyright on this work, we have licenced it under the MIT licence.

Bairwell Ltd: http://www.bairwell.com / Twitter: http://twitter.com/bairwell
Richard Chiswell: http://blog.rac.me.uk / Twitter: http://twitter.com/rchiswell

Licence
=======
This work is licensed under the MIT License. Please see the included LICENSE.txt file for information.

Copyright (c) 2011 Bairwell Ltd - http://www.bairwell.com

Installation
============
The easiest way to install this module is to install it via PEAR
    pear channel-discover pear.bairwell.com
    pear install bairwell/autoloader
Or to install the dist/Bairwell_Autoloader...tgz file
    pear install dist/Bairwell_Autoloader*.tgz

Basic Usage
===========
include 'Bairwell/Autoloader.php';
$autoloader = new \Bairwell\Autoloader();
spl_autoload_register(array($autoloader, 'load'));

and then just start calling your classes.

Advanced Usage
==============
Documentation is included in build/docblox

To build
========
This is a PHIX ( http://phix-project.org ) compatible module and if you have PHIX installed you
can just do:
    phing test          <- to run the unit tests
    phing code-review   <- get code quality information
    phing phpdoc        <- get PhpDocumentor docs
    phing pear-package  <- to generate the package

This module has also been built with the Jenkins CI in mind ( http://www.jenkins-ci.org/ ) and
you can use the build.jenkins.xml file to run the tests and update Jenkins.

Changelog
=========
0.4: Now just returns FALSE instead of throwing exceptions when class is not found to ensure
     compatibility with other autoloaders.

0.3: Fix tracker issue #1 where we were using the first namespace separator instead of last
     Add debug system

0.2: Added work around for https://github.com/sebastianbergmann/phpunit/issues/422

0.1: First Release